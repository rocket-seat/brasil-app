const mongoose = require('mongoose');

const PoliticianSchema = new mongoose.Schema({
  externalId: {
    type: Number,
    required: true,
    unique: true,
  },
  displayName: {
    type: String,
    required: true,
    trim: true,
  },
  photo: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
    trim: true,
  },
  siglaPartido: {
    type: String,
  },
  siglaUf: {
    type: String,
  },
  uriPartido: {
    type: String,
  },
  legalProcess: [{ type: mongoose.Schema.ObjectId, ref: 'LegalProcess' }],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

mongoose.model('Politician', PoliticianSchema);
