const mongoose = require('mongoose');

const LegalProcessSchema = new mongoose.Schema({
  nup: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    required: true,
    trim: true,
  },
  link: {
    type: String,
    required: true,
    trim: true,
  },
  politician: { type: mongoose.Schema.ObjectId, ref: 'Politician' },
  externalId: {
    type: Number,
    required: true,
    unique: true,
  },
  fileUrl: {
    type: String,
    trim: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },

});

mongoose.model('LegalProcess', LegalProcessSchema);
