const express = require('express');
const requireDir = require('require-dir');

const routes = express.Router();

const authMiddleware = require('./middlewares/auth');

const controllers = requireDir('./controllers');
/** No need authentication */

routes.get('/politicians', controllers.politicianController.listAll);
routes.get('/congressman', controllers.politicianController.listCongressmans);
// routes.get('/senator', controllers.politicianController.listSenators);
routes.get('/process/:id', controllers.legalProcessController.byPolitician);

/** Route need Authentication */
routes.use(authMiddleware);

/** Politicians */
routes.post('/politician/create', controllers.politicianController.create);
routes.delete('/politician/:id', controllers.politicianController.destroy);
routes.get('/politicians/', controllers.politicianController.listAll);

/** Legal Process */
routes.post('/process/create/:politicianId', controllers.legalProcessController.create);
routes.delete('/process/:id', controllers.legalProcessController.destroy);
routes.get('/process/:id', controllers.legalProcessController.byPolitician);


module.exports = routes;
