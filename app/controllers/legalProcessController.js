const mongoose = require('mongoose');

const LegalProcess = mongoose.model('LegalProcess');
const Politician = mongoose.model('Politician');

module.exports = {
  async create(req, res, next) {
    try {
      const politician = await Politician.findById(req.params.politicianId);
      if (!politician) {
        return res.json({ error: 'Politician not found' });
      }
      const legalProcess = await LegalProcess.create({ ...req.body, politician: req.params.politicianId, externalId: politician.externalId });
      politician.legalProcess.push(legalProcess._id);
      politician.save();
      return res.json(legalProcess);
    } catch (err) {
      return next(err);
    }
  },

  async destroy(req, res, next) {
    try {
      const legalProcess = await LegalProcess.findByIdAndRemove(req.params.id);
      const politician = await Politician.findById(legalProcess.politician);
      if (politician) {
        const index = politician.legalProcess.indexOf(req.params.id);
        if (index !== -1) {
          politician.legalProcess.splice(index, 1);
          politician.save();
        }
      }
      return res.json({ status: 'Excluido' });
    } catch (err) {
      return next(err);
    }
  },

  async byPolitician(req, res, next) {
    try {
      const legalProcess = await LegalProcess.find({ externalId: req.params.id });
      const politician = await Politician.find({ externalId : req.params.id });
      let corrupt = false;
      if (politician.length > 0) {
        corrupt = true;
      }

      return res.json({
        legalProcess,
        total: legalProcess.length,
        corrupto: corrupt,
      });
    } catch (err) {
      return next(err);
    }
  },
};
