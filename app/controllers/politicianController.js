const mongoose = require('mongoose');

const Politician = mongoose.model('Politician');

module.exports = {
  async create(req, res, next) {
    try {
      const politician = await Politician.create({ ...req.body });
      return res.json(politician);
    } catch (err) {
      return next(err);
    }
  },

  async destroy(req, res, next) {
    try {
      await Politician.findByIdAndRemove(req.params.id);
      return res.send();
    } catch (err) {
      return next(err);
    }
  },

  async listAll(req, res, next) {
    try {
      const data = await Politician.find();
      return res.json(data);
    } catch (err) {
      return next(err);
    }
  },

  async listCongressmans(req, res, next) {
    try {
      const data = await Politician.find({ type: 'Deputado' });
      return res.json(data);
    } catch (err) {
      return next(err);
    }
  },

};
