const authConfig = require('../../config/auth');

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ error: 'No token provided' });
  }

  if (authHeader === authConfig.secret) {
    return next();
  }
  return res.status(401).json({ error: 'Token invalid' });
};
