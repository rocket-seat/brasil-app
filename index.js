const app = require('express')();
const mongoose = require('mongoose');
const requireDir = require('require-dir');
const bodyParser = require('body-parser');
const Raven = require('./app/services/sentry');

const dbConfig = require('./config/database');

mongoose.connect(dbConfig.url, { autoIndex: false });
requireDir(dbConfig.modelsPath);

app.use(cors());

app.use(bodyParser.json());
app.use(Raven.requestHandler());


app.use('/api', require('./app/routes'));

app.use(Raven.errorHandler());
// app.post('/create', async (req, res) => {
//   const User = mongoose.model('User');

//   console.log(req.body);

//   await User.create(req.body);

//   return res.send();
// });

app.listen(3000);
