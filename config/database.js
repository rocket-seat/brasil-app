const path = require('path');

module.exports = {
  url: 'mongodb://localhost/brasiliapp',
  modelsPath: path.resolve('app', 'models'),
};
